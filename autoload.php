<?php

class __init{

    protected $classes;

    function __construct(){

        spl_autoload_register( array ($this, 'autoload') );

        $this->mapper( __DIR__ );

        $this->init();

    }

    protected function mapper( $dir ){

        $map = scandir( $dir );
        
        $ignore = ['views','.','..'];
        
        foreach( $map as &$item){

            if(in_array($item, $ignore) ) continue;

            if( strpos($item, '.php') ) $this->classes[str_replace ('.php', '', $item)] = $dir . '/' . $item;

            elseif ( is_dir( $dir.'/'.$item ) ){

                $this->mapper(  $dir.'/'.$item  );

            }

        }

    }

    public function autoload( $class ){

        if( isset($this->classes[$class]) &&  file_exists( $this->classes[$class] ) ){

            require_once(  $this->classes[$class] );

        }

    }

    protected function init(){
       /*echo "<pre>";
       print_r($this->classes);
       echo "</pre>";*/
    }

}

new __init();
