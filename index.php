<?php

/******************************************************************************
*  Автозагрузчик для всех классов и других автозагрузчиков
******************************************************************************/

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

include (__DIR__.'/autoload.php');

$GET = (object)filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);
$POST = (object) filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);

if( !isset( $GET->controller)){
    $GET->controller = 'main';
}

$controller = new $GET->controller();

if( isset($GET->method)){
    $method = $GET->method;
    $controller->$method();
    die();
}
if( isset($POST->method)){
    $method = $POST->method;
    $controller->$method();
    die();
}

$controller->index();
die();