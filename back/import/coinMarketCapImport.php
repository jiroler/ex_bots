<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

class coinMarketCapImport {

    protected $base;

    public function __construct() {
        $this->base = "https://api.coinmarketcap.com/v2/";
    }

    public function parse_coins() {
        $url = $this->base . "listings/";
        $res = json_decode(file_get_contents($url));
        $coinsModel = new infoCoinsModel();
        if (!empty($res->data)) {
            foreach ($res->data as $coin) {
                // проверяем, есть ли монета в базе
                $check_coin = $coinsModel->get("SELECT id FROM {$coinsModel->get_table()} WHERE cmc_slug = \"{$coin->website_slug}\"");
                //Новая строка
                //var_dump($check_coin); exit;
                if (empty($check_coin)) {
                    $coinsModel->insert_row([
                        'name' => $coin->name,
                        'symbol' => $coin->symbol,
                        'cmc_slug' => $coin->website_slug]);
                } else {
                    $coinsModel->update_row([
                        'name' => $coin->name,
                        'symbol' => $coin->symbol,
                        'cmc_slug' => $coin->website_slug], ['id' => $check_coin[0]['id']]);
                }
            }
        }
    }

    public function parse_coin($slug) {
        $url = "https://coinmarketcap.com/currencies/{$slug}";
        $html = file_get_contents($url);
        $exchanges = [];
        //preg_match_all("/<a.*?class=(\"|')link-secondary.*?<\/a>/s", $html, $table_arr);
        preg_match_all("/<table.*?id=(\"|')markets-table.*?<\/table>/s", $html, $table_arr);
        if (!empty($table_arr[0][0])) {
            preg_match_all("/<a.*?class=(\"|')link-secondary.*?<\/a>/", $table_arr[0][0], $exchanges_arr);
            if (!empty($exchanges_arr[0])) {
                foreach ($exchanges_arr[0] as $exch) {
                    $exch = str_replace("<a class=\"link-secondary\" href=\"/exchanges/", "", $exch);
                    $exchange = substr($exch, 0, strpos($exch, '/'));
                    $exchange_name = substr($exch, strpos($exch, '>') + 1, strpos($exch, '<') - strpos($exch, '>') - 1);
                    if (!isset($exchanges[$exchange])) {
                        $exchanges[$exchange] = $exchange_name;
                    }
                }
            }
        }
        return ["exchanges" => $exchanges];
    }
    
    public function get_coins_exchanges(){
        
        $inc = 0;
        $coins_info = [];
        $new_exchanges = [];
        $exchanges_ids = [];
        
        $coinsModel = new infoCoinsModel();
        $exchangeModel = new infoExchangeModel();
        $coinExchangesModel = new infoCoinExchangesModel();
        
        $coins = $coinsModel->get_list();
        
        foreach($coins as $coin){
            
            $inc++;
            $exchanges = $this->parse_coin($coin['cmc_slug'])['exchanges'];
            
            if (count($exchanges)){
                
                $coins_info[$coin['id']] = $exchanges;
                $new_exchanges = array_merge($new_exchanges, $exchanges);
                
            }else{
                echo 'Alarm! Not exchanges. May be DDOS =('.PHP_EOL;
            }
            
            echo $inc.' / '.count($coins).') '.$coin['symbol'].': '.count($exchanges).PHP_EOL;
            usleep(500000);
        }
        
        $inc = 0;
        
        foreach($new_exchanges as $slug => $name){
            
            $check_exchange = $exchangeModel->get_where('slug = "'.$slug.'"');    
            
            if (empty($check_exchange)) {
            
                $exchangeModel->insert_row([
                    'name' => $name,
                    'slug' => $slug,
                ]);
                
                $inc++;
            }
        }
        
        echo PHP_EOL.$inc.' new exchanges inserted'.PHP_EOL;
        
        $exchanges_list = $exchangeModel->get_list();
        
        foreach($exchanges_list as $exchange){
            $exchanges_ids[$exchange['slug']] = $exchange['id'];
        }
        
        $inc = 0;
        
        foreach($coins_info as $coin_id => $exchanges){
            foreach($exchanges as $slug => $name){
                
                $check_coin_exchange = $coinExchangesModel->get_where('coin_id = "'.$coin_id.'" AND exchange_id = "'.$exchanges_ids[$slug].'"');
                
                if (empty($check_coin_exchange)) {
                    
                    $coinExchangesModel->insert_row([    
                        'coin_id' => $coin_id,
                        'exchange_id' => $exchanges_ids[$slug],
                    ]);
                    
                    $inc++;
                }
            }
        }
        
        echo PHP_EOL.$inc.' new coins_exchanges inserted'.PHP_EOL;
        
        echo PHP_EOL.'finish!'.PHP_EOL;
    }

}
