<?php

include str_replace("/back/import", "", __DIR__) . "/autoload.php";

if (empty($argv[1])) {
    die("No controller");
}

if (!class_exists($argv[1])) {
    die("Class {$argv[1]} not exists" . PHP_EOL);
}

$controller = new $argv[1];

if (!isset($argv[2])) {
    die();
}

if (!method_exists($controller, $argv[2])) {
    die("Class {$argv[1]} has no method \"{$argv[2]}\"" . PHP_EOL);
}
$method = $argv[2];

if(isset($argv[3])){
    $controller->$method($argv[3]);
}else{
    $controller->$method();
}

