<?php

class infoExchangeModel extends baseModel {

    public function __construct() {
        parent::__construct("info");
        $this->set_table("exchanges");
    }

}
