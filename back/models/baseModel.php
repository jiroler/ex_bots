<?php

class baseModel{

    protected $db;
    protected $table;

    function __construct( $dbname = null) {
        $this->db = (new db($dbname) )->instance();
    }

    public function set_table( $table ){
        $this->table = $table;
    }
    
    public function get($query){
        return $this->get_result($this->db->query($query));
    }
    
    public function  get_table(){
        return $this->table;
    }

    public function get_list(){
        $res = $this->db->query("SELECT * FROM {$this->table}");
        return $this->get_result($res);
    }
    
    public function get_where($where){
        $res = $this->db->query("SELECT * FROM {$this->table} WHERE ".$where);
        return $this->get_result($res);
    }

    public function get_result( $res ){

        $obj = [];
        if(!is_object($res)) return $res;
        if( $res->num_rows > 0 ){ //todo тут может прийти что-то не то!
            while( $row = $res->fetch_assoc() ){
                $obj[] = $row;
            }
        }
        return $obj;
    }

    public function update_row( $fields = [], $where = []){
        //var_dump($fields);
        //var_dump($where);

        $update_fields = [];
        $update_where = [];
        foreach($fields as $field_name => $field_value ){
            $update_fields[] = " {$field_name} = '{$field_value}'";
        }
        foreach($where as $field_name => $field_value ){
            $update_where[]= " {$field_name} = '{$field_value}'";
        }

        $sql = "UPDATE {$this->table} SET ".implode(",",$update_fields)." WHERE ".implode(" AND ", $update_where);
        //print_r($sql);
        $res = $this->db->query($sql);
        return $res;

    }

    public function insert_row($data){

        $fields = implode(",",array_keys($data));
        $val = "'".implode("','",$data)."'";
        $sql = "INSERT INTO {$this->table} ({$fields}) VALUES ({$val})";
        //print_r($sql);
        $res = $this->db->query($sql);
        return $res;
    }
    
    public function replace_row($data){

        $fields = implode(",",array_keys($data));
        $val = "'".implode("','",$data)."'";
        $sql = "REPLACE INTO {$this->table} ({$fields}) VALUES ({$val})";
        $res = $this->db->query($sql);
        return $res;
    }
    
    public function delete($where = []){
        
        $where = (!empty($where))?"WHERE ".implode(" AND ",$where):"";
        $sql = "DELETE FROM {$this->table} {$where}";
        //echo $sql;
        $res = $this->db->query($sql);
        return $res;
    }
    
    public function insert_id(){
        return $this->db->insert_id;
    }
    
    public function affected_rows(){
        return $this->db->affected_rows;
    }
    
    public function query($query){
        return $this->db->query($query);
    }



}

