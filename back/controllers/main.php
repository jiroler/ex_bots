<?php

class main extends baseController {

    function __construct() {
        parent::__construct();
    }

    public function index() {

        $GET = (object) filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);
        $this->viewData['JS'][10] = "/arbot/front/assets/js/Chart.min.js";
        $data = [
            'type' => 'line',
            'data' => [
                'labels' => ['Позавчера', 'Вчера', 'Сегодня', 'Завтра', 'Послезавтра'],
                'datasets' => []
            ],
            'options' => []
        ];
        $colors = ['eth' => '#cc51ff', 'btc' => '#f99300', 'rur' => '#8a4038', 'usd' => '#4cc947'];
        foreach (['eth', 'btc', 'rur', 'usd'] as $cur) {
            $data['data']['datasets'][] = [
                'label' => $cur,
                'data' => [rand(1,1000)/1000,rand(1,1000)/1000,rand(1,1000)/1000,rand(1,1000)/1000,rand(1,1000)/1000,],
                'borderColor' => $colors[$cur],
                'backgroundColor' => 'transparent',
                'borderWidth' => 2
            ];
        }
        $this->viewData['data'] = $data;
        $this->view("common/header");
        $this->view("pages/main");
        $this->view("common/footer");
        echo $this->html;
    }

}
