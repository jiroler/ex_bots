<?php

class baseController {

    static $viewDir;
    protected $viewData;
    protected $html;
    protected $root;
    protected $banderSays;

    function __construct() {
        self::$viewDir = str_replace("back/controllers", "front/views/", __DIR__);
        $this->viewData = [];
        $this->html = "";
        $this->banderSays = [
            "Я построю свой лунный модуль, с блэкджеком и шлюхами! И вообще, к черту лунный модуль и блэкджек! Ай, к чёрту всё!",
            "Как и многие жизненные проблемы, эту можно решить сгибанием.",
            "Bite my shiny metal ass",
            "Привет, начинки для гробов!",
            "Я люблю вас, мешки с мясом!",
            "Дорогая, хочешь убить всех людей?",
            "Кто вы такие и почему это должно меня волновать?!",
            "Пиво рулит, не ты!",
            "Слава роботам! Убить всех человеков!",
            "Все, я богат! Пока, неудачники! Я вас всегда ненавидел!",
            "Да! Я богат! Правда ты тоже, но это почему то не радует.",
            "Если бы я верил, что после смерти меня ждет другая жизнь, я бы убил себя прямо сейчас.",
            "Асталависта, мешок с костями!",
            "Я умираю. Мне нужно искусственное дыхание рот в зад!",
            "Я хочу жить! Я ещё много чего не украл!",
            "Я не так знаменит, чтобы давить людей безнаказанно.",
            "Хочешь я немного поглумлюсь над трупом?",
            "Не забывайте про правила хорошего тона. Вилки в левый карман, ложки — в правый!",
            "Давайте смотреть реально. Комедия — мёртвый жанр, а трагедия — это смешно!",
            "Смотреть порно и зарабатывать?! Что-то мне не верится.",
            "Шантаж звучит как-то вульгарно, мне больше нравится вымогательство.",
            "Не бейте меня!.. я предам кого угодно!",
            "Эй, я же не говорю тебе, как говорить мне, что делать, так что не говори мне, как делать то, что ты говоришь мне сделать!",
            "Вы можете сказать, что я болтаю ерунду. Зато какую!",
            "Он съел пищу, которую я приготовил, и по случайному совпадению его желудок разорвало…",
            "Мне так стыдно !!! я хочу что бы все умерли !"
        ];
        $this->viewData['bander'] = $this->banderSays[rand(0,25)];
        $this->viewData['CSS'][9998] = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css";
        $this->viewData['CSS'][9999] = "/arbot/front/assets/css/style.css";
        $this->viewData['JS'][9998] = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js";
        $this->viewData['JS'][9999] = "/arbot/front/assets/js/common.js";
    }

    public function set_fond($fond) {
        $this->fond = $fond;
    }

    public function view($name, $return = false) {

        $file = self::$viewDir . $name . ".php";
        extract($this->viewData);
        $html = "";
        if (file_exists($file)) {
            ob_start();
            include $file;
            $html = ob_get_clean();
        }
        if ($return) {
            return $html;
        }
        $this->html .= $html;
    }

    public function set_secure_tocken($key) {
        session_start();
        $tocken = md5(time() . $key);
        $_SESSION[$key] = $tocken;
        return $tocken;
    }

    public function proof_secure_tocken($key, $tocken) {
        session_start();
        return (isset($_SESSION[$key]) && $_SESSION[$key] === $tocken);
    }

}
