$('document').ready(function () {     
    $('.chart').each(function () {
        console.log('123');
        var _ = $(this)[0].getContext('2d');
        var data = $(this).data('data');
        if (!mcDebug(data, 'Не хватает атрибута data-data у canvas >> ')) {
            return false;
        }

        Chart.defaults.global.defaultFontColor = '#CCC';
        Chart.defaults.global.defaultFontFamily = 'Roboto, Arial, serif';
        Chart.defaults.global.elements.line.tension = 0;
        Chart.defaults.global.elements.arc.borderWidth = 3;
        Chart.defaults.global.elements.point.radius = 3;
        Chart.defaults.global.legend.position = 'right';
        var ch = new Chart(_, data);

    });
});


var mcDebug = function (object, text) {

    var message = (text === undefined) ? "" : text + "Object: " + object;
    if (object === undefined) {
        if (message !== "") {
            console.log(message);
        }
        return false;
    } else {

        if (object.length === 0) {
            if (message !== "") {
                console.log(message);
            }
            return false;
        }
    }
    return true;
}

function d(str) {
    console.log(str);
}

