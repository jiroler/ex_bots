<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <!--meta http-equiv="Refresh" content="120" /-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= (isset($title)) ? $title : "Арбот" ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Арбитраж бирж" />
        <meta name="keywords" content="" />

        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Theme style  -->

        <?php if (isset($CSS)): ?>
            <?php foreach ($CSS as $link): ?>
                <link rel="stylesheet" href="<?= $link; ?>">
            <?php endforeach; ?>
        <?php endif; ?>
         
    </head>
    <body>
        <div class="bot-panel">
            <div class="bot-left">
                <div class="bot-logo">
                    <a href="/arbot/index.php" title="<?=$bander?>">
                        <img src="/arbot/front/assets/i/l.png"/>
                        <header>arbot</header>
                    </a> 
                </div>
                <div class="bot-menu">
                    <a href="/arbot/index.php" class="active">Главная</a>
                    <a href="#">Доход за день</a>
                </div>
            </div>
            <div class="bot-body">
                <div class="bot-body-top-panel">
                    <div class="breadcrumbs">Главная</div>
                </div>
                

